# Features

- Polygon Tool
- Stamp Tool
- Select Tool
- Path Tool
- Spray Stamp Tool
- Stamp Along Line Tool
- Draw Shapes Tool
- Text Tool
- Gradient Tool
- Sketch Tool
- Map Layers
- Grid
- Help Lines
- Snap to Path/Polygon
- Lighting
- Shaders
- Libraries (Stamp vs Texture)
- Tool Presets
- Copy/Paste
- Multi-Floor Maps
- Boolean-Polygons
- Path-Cutouts
- Group Objects
- Hide Objects/Layers/Groups
- Freeze Objects/Layers/Groups
- Selection Masks (How to select the correct object)
- Rendering Pipeline
- Print Map
- Remember Tool Settings After Edit (Keep tool settings after selection tool is used)
- Object Tree
- Select Object from Tree
- Undo/Redo
- Defined Views (Save Zoom and Position for quick map/area overview)
- Background Color/Texture
- Transparent Background Renderer
- Map Templates
- Map Units (For Map Size, Grid and Token Scaling)

## Categorization

### Tools (Bottom Left Panel)

- Polygon Tool
  - Boolean-Polygons
- Stamp Tool
  - Spray Stamp Tool
  - Stamp Along Line Tool
- Select Tool
- Path Tool
  - Path-Cutouts
- Draw Shapes Tool
  - Circle
  - Rectangle
- Text Tool
- Gradient Tool
- Light Tool
- Sketch Tool
- Tool Presets
- Remember Tool Settings After Edit (Keep tool settings after selection tool is used)

### Libraries (Top Left Panel)

- Libraries (Stamp vs Texture)
- Import Textures
- Default Scaling

### Object Tree (Top Right Panel)

- Object Tree
- Select Object from Tree
- Map Layers
- Group Objects
- Hide Objects/Layers/Groups
- Freeze Objects/Layers/Groups
- Multi-Floor Maps
- Copy/Paste

### Layer Properties (Bottom Right Panel)

- Shaders

### Map Viewport/Canvas (Center Panel)

- Grid
- Help Lines
- Snap to Path/Polygon
- Map Units (For Map Size, Grid and Token Scaling)
- Selection Masks (How to select the correct object)

### Import Export Map (Menu)

- Rendering Pipeline
- Print Map
- Background Color/Texture
- Transparent Background Renderer
- Map Templates

### Other

- Defined Views (Save Zoom and Position for quick map/area overview)
- Undo/Redo

## Diagrams

![](diagrams/SelectPolygon.drawio.png)
![](diagrams/PolygonStates.drawio.png)