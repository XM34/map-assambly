using Godot;
using System;
using System.Collections.Generic;

public class TextureImport : Node
{
    /// <summary>
    /// Traverses a directory and all its subdirectories recursively and returns a list of all files in those directories
    /// </summary>
    /// <param name="path">The path to the directory</param>
    /// <param name="byExtension">Only selects file with the given extension. E.g. "png"</param>
    /// <returns>A list of all files in the directory and its subdirectories</returns>
    public List<string> GetFilesInDir(string path, string byExtension = "")
    {
        var result = new List<string>();

        Directory dir = new Directory();
        if (dir.Open(path) != 0)
        {
            GD.PrintErr("Error: Couldn't open resource directory!");
        }
        dir.ListDirBegin(true);

        for (var fileName = dir.GetNext(); fileName != ""; fileName = dir.GetNext())
        {
            if (dir.CurrentIsDir())
            {
                result.AddRange(GetFilesInDir(dir.GetCurrentDir() + "/" + fileName, byExtension));
            }
            else if (fileName.Extension() == byExtension)
            {
                result.Add(dir.GetCurrentDir() + "/" + fileName);
            }
            else
            {
                GD.Print("Extension detected: " + fileName.Extension());
            }
        }

        dir.ListDirEnd();

        return result;
    }


    /// <summary>
    /// Imports an image and saves it as a resource
    /// </summary>
    /// <param name="imagePath">Path to the imported image</param>
    /// <param name="resourcePath">Path to save the resource to</param>
    public void ImportImage(string imagePath, string resourcePath)
    {
        GD.Print("Importing \"" + imagePath + "\"");

        var image = new Image();
        var texture = new ImageTexture();

        if (image.Load(imagePath) != 0)
            GD.PrintErr("Couldn't load image");
        texture.CreateFromImage(image);
        texture.Flags = 7; //Repeat, Mipmaps and Magnifying filter

        // Save as resource
        if (ResourceSaver.Save(resourcePath, texture, ResourceSaver.SaverFlags.ChangePath) != 0)
            GD.PrintErr("Couldn't save resource to file \"" + resourcePath + "\"");
    }

    /// <summary>
    /// Constructs a resource path to a file
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    public string GetResourcePath(string filePath)
    {
        string fileName = filePath.GetFile();
        string basePath = filePath.GetBaseDir();

        return basePath + "/.import/" + fileName + ".tres";
    }
}
