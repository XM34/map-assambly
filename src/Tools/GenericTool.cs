using Godot;
using System;

public class GenericTool : Node
{
    private bool enabled = false;
    public bool Enabled
    {
        get { return enabled; }
        set
        {
            enabled = value;

            if (value)
            {
                var props = GetNode<GenericToolProps>(GetNode<ToolManager>("/root/ToolManager").ActiveToolProps);
                props.Connect(nameof(GenericToolProps.PropsChanged), this, nameof(_OnPropsChanged));
                _OnEnabled();
            } else {
                var props = GetNode<GenericToolProps>(GetNode<ToolManager>("/root/ToolManager").ActiveToolProps);
                props.Disconnect(nameof(GenericToolProps.PropsChanged), this, nameof(_OnPropsChanged));
                _OnDisabled();
            }
        }
    }

    public virtual void _OnPropsChanged() {}

    protected virtual void _OnEnabled() {}
    protected virtual void _OnDisabled() {}
}