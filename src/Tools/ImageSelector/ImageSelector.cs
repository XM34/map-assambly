using Godot;
using System;
using System.Collections.Generic;

public class ImageSelector : GridContainer
{
	const string CONTENT_DIR = "res://temp/Objects";

	[Signal] public delegate void TextureSelected(string path);

	private PackedScene TextureItemScn = ResourceLoader.Load<PackedScene>("res://src/Tools/ImageSelector/TextureItem.tscn");



	public override void _Ready()
	{
		TextureImport textureImport = GetNode<TextureImport>("/root/TextureImport");

		GD.Print("Importing images...");
		var textureFiles = textureImport.GetFilesInDir(CONTENT_DIR, "png");
		textureFiles.AddRange(textureImport.GetFilesInDir(CONTENT_DIR, "jpg"));
		GD.Print(textureFiles.Count + " files found in directory");

		// Create ".import" folder if not existing
		var contentDir = new Directory();
		contentDir.Open(CONTENT_DIR);
		contentDir.MakeDir(".import");

		foreach(var textureFile in textureFiles) {
			var resourcePath = textureImport.GetResourcePath(textureFile);
			textureImport.ImportImage(textureFile, resourcePath);

			// Create and display texture preview
			var textureItem = TextureItemScn.Instance<TextureItem>();
			textureItem.Connect("Pressed", this, nameof(OnSelectTexture));
			textureItem.Texture = ResourceLoader.Load<ImageTexture>(resourcePath);
			textureItem.TexturePath = resourcePath;
			textureItem.HintTooltip = resourcePath;

			AddChild(textureItem);
		}
	}

	public void OnSelectTexture(string path)
	{
		GD.Print("Selected Texture: " + path);
		EmitSignal(nameof(TextureSelected), path);
	}
}
