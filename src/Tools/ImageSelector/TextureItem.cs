using Godot;
using System;

public class TextureItem : TextureRect
{
    [Signal] public delegate void Pressed(string path);

    [Export] public string TexturePath = "";

    public override void _GuiInput(InputEvent @event)
    {
        base._GuiInput(@event);

        if (@event is InputEventMouseButton)
        {
            var mbEvent = @event as InputEventMouseButton;
            if (mbEvent.ButtonIndex == ((decimal)ButtonList.Left) && mbEvent.Pressed)
            {
                EmitSignal(nameof(Pressed), TexturePath);
            }
        }
    }
}
