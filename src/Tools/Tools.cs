using Godot;
using System;

public class Tools : Control
{
    public Node currentTool = null;
	public string currentTexturePath = "";

    [Export] public NodePath selectToolProperties = null;
    [Export] public NodePath polygonToolProperties = null;
    [Export] public NodePath pathToolProperties = null;
    [Export] public NodePath objectToolProperties = null;

    public override void _Ready()
    {
        var toolManager = GetNode<ToolManager>("/root/ToolManager");
        toolManager.RegisterToolPropertiesPath("ObjectTool", GetNode<Control>(objectToolProperties).GetPath());
        toolManager.CallDeferred("ChangeActiveTool", "ObjectTool");
    }

    public void _on_select_tool_selected()
    {
        GD.Print("The select tool is not yet implemented");
        GetNode<ToolManager>("/root/ToolManager").ChangeActiveTool("SelectTool");
    }

    public void _on_path_tool_selected()
    {
        GD.Print("The path tool is not yet implemented");
        GetNode<ToolManager>("/root/ToolManager").ChangeActiveTool("PathTool");
    }

    public void _on_polygon_tool_selected()
    {
        GD.Print("The polygon tool is not yet implemented");
        GetNode<ToolManager>("/root/ToolManager").ChangeActiveTool("PolygonTool");
    }

    public void _on_object_placement_tool_selected()
    {
        GetNode<ToolManager>("/root/ToolManager").ChangeActiveTool("ObjectTool");
    }

    public void _OnTextureSelected(string path)
    {
		this.currentTexturePath = path;
    }
}
