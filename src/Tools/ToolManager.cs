using Godot;
using System;
using System.Collections.Generic;

public class ToolManager : Node
{
    private Dictionary<String, ToolPaths> ToolPathDict;
    public NodePath ActiveTool { get; set; }
    public NodePath ActiveToolProps { get; set; }


    public override void _Ready()
    {
        ToolPathDict = new Dictionary<string, ToolPaths>();
    }

    public void RegisterToolPropertiesPath(string toolName, NodePath propertiesPath)
    {
        if (!ToolPathDict.ContainsKey(toolName))
        {
            ToolPathDict.Add(toolName, new ToolPaths());
        }

        ToolPathDict[toolName].Properties = propertiesPath;
    }

    public void RegisterToolPath(string toolName, NodePath toolPath)
    {
        if (!ToolPathDict.ContainsKey(toolName))
        {
            ToolPathDict.Add(toolName, new ToolPaths());
        }

        ToolPathDict[toolName].Tool = toolPath;
    }

    public void ChangeActiveTool(string toolName)
    {
        foreach (var keyVal in ToolPathDict)
        {
            if (keyVal.Key == toolName)
            {
                GetNode<CanvasItem>(keyVal.Value.Properties).Show();

                var tool = GetNode(keyVal.Value.Tool);

                ActiveTool = keyVal.Value.Tool;
                ActiveToolProps = keyVal.Value.Properties;

                GetNode<GenericTool>(keyVal.Value.Tool).Enabled = true;
            }
            else
            {
                GetNode<CanvasItem>(keyVal.Value.Properties).Hide();

                var tool = GetNode(keyVal.Value.Tool);

                GetNode<GenericTool>(keyVal.Value.Tool).Enabled = false;
            }
        }
    }

    class ToolPaths
    {
        public NodePath Properties { get; set; }
        public NodePath Tool { get; set; }
    }
}
