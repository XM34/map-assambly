using Godot;
using System;

public class ObjectTool : GenericTool
{
    private const float SCALE_MODIFIER = 0.01f;

    // Todo move to CanvasContainer and update on tool change
    private bool mouseOnCanvas = false;

    public override void _Ready()
    {
        GetNode<ToolManager>("/root/ToolManager").RegisterToolPath("ObjectTool", this.GetPath());
    }

    public void _MouseEntered()
    {
        mouseOnCanvas = true;
        if (Enabled) GetNode<Node2D>("ObjectPreview").Show();
    }

    public void _MouseExited()
    {
        mouseOnCanvas = false;
        GetNode<Node2D>("ObjectPreview").Hide();
    }

    protected override void _OnEnabled()
    {
        base._OnEnabled();
        GetNode<Node2D>("ObjectPreview").Show();
        GD.Print("Enable Object Tool");
    }

    protected override void _OnDisabled()
    {
        base._OnDisabled();
        GetNode<Node2D>("ObjectPreview").Hide();
        GD.Print("Disable Object Tool");
    }

    public override void _Input(InputEvent @event)
    {
        base._Input(@event);

        if (!mouseOnCanvas || !Enabled)
            return;

        if (@event is InputEventMouseButton mouseEvent)
        {
            // Place object on left mouse button release
            if (mouseEvent.ButtonIndex == (int)ButtonList.Left && mouseEvent.Pressed == false)
            {
                var mousePos = GetNode<Node2D>("ObjectPreview").Position;
                SpawnObjectAtPos(mousePos);
            }
        }

        if (@event is InputEventMouseMotion motionEvent)
        {
            var preview = GetNode<Node2D>("ObjectPreview");
            var props = GetNode<ObjectToolProps>(GetNode<ToolManager>("/root/ToolManager").ActiveToolProps);
            var relativeMousePos = preview.GetGlobalMousePosition() - preview.Position;

            if (Input.IsActionPressed("rotate_object"))
            {
                var rotation = (int)Mathf.Rad2Deg(relativeMousePos.Angle()) - 90;
                props.Rotation = rotation;
            }
            else if (Input.IsActionPressed("scale_object"))
            {
                var scale = (float)Math.Exp(-relativeMousePos.y * SCALE_MODIFIER);
                props.Scale = scale;
            }
        }
    }

    private void SpawnObjectAtPos(Vector2 position)
    {
        var toolManager = GetNode<ToolManager>("/root/ToolManager");
        var properties = GetNode<ObjectToolProps>(toolManager.ActiveToolProps);
        var sprite = new Sprite();

        sprite.Texture = properties.Texture;
        sprite.Position = position;
        sprite.RotationDegrees = properties.Rotation;
        sprite.Scale = Vector2.One * properties.Scale;
        sprite.AddToGroup("map_elements");
        sprite.AddToGroup("placable_objects");
        GD.Print("Scale: ", sprite.Scale, properties.Scale);
        GetNode("../../MapRoot").AddChild(sprite);
        sprite.Owner = GetNode("../../MapRoot");
    }

    public override void _OnPropsChanged()
    {
        base._OnPropsChanged();

        var props = GetNode<ObjectToolProps>(GetNode<ToolManager>("/root/ToolManager").ActiveToolProps);
        var preview = GetNode<Sprite>("ObjectPreview");

        preview.Texture = props.Texture;
        preview.RotationDegrees = props.Rotation;
        preview.Scale = Vector2.One * props.Scale;
    }
}
