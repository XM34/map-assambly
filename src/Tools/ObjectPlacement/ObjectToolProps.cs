using Godot;
using System;

public class ObjectToolProps : GenericToolProps
{
    private Texture texture;
    public Texture Texture
    {
        get { return GetNode<TextureRect>("CenterContainer/SelectTexture/SelectedTexture").Texture; }
        set { GetNode<TextureRect>("CenterContainer/SelectTexture/SelectedTexture").Texture = value; }
    }
    public int Rotation
    {
        get { return (int)GetNode<SpinBox>("TextProperties/RotationValue").Value; }
        set { GetNode<SpinBox>("TextProperties/RotationValue").Value = value; }
    }

    public float Scale
    {
        get { return (float)GetNode<SpinBox>("TextProperties/ScaleValue").Value; }
        set { GetNode<SpinBox>("TextProperties/ScaleValue").Value = value; }
    }


    public void _OnTextureSelected(string texturePath)
    {
        Texture = ResourceLoader.Load<Texture>(texturePath);
        _OnValueChanged();
    }

    public void _OnValueChanged(params dynamic[] args)
    {
        EmitSignal(nameof(PropsChanged));
    }
}
