extends HBoxContainer

enum FileMenuId {NEW, SAVE, LOAD, EXPORT_AS}
onready var file_menu_button : MenuButton
onready var edit_menu : MenuButton = $EditMenu
onready var file_menu : PopupMenu
onready var exportMapRoot : Node
export var MapRoot : NodePath
export var exportViewPort : NodePath
export var viewPort : NodePath

# Called when the node enters the scene tree for the first time.
func _ready():
	file_menu_button = $FileMenu
	
	setup_file_menu()
#	edit_menu.get_popup().add_item("Undo", 0)


func setup_file_menu() -> void:
	# set shortcuts for actions
	var file_menu_items := { # order as in FileMenuId enum
		"New..." : 0,
		"Save..." : InputMap.get_action_list("save")[0].get_scancode_with_modifiers(),
		"Load..." : 0,
		"Export as..." : 0,
	}
	
	#add menuitems to menu
	file_menu = file_menu_button.get_popup()
	var i := 0
	for item in file_menu_items.keys():
		file_menu.add_item(item, i, file_menu_items[item])
		i+=1
	
	#link functions to buttons
	var _connect_error = file_menu.connect("id_pressed", self, "file_menu_id_pressed")

# selector which function is executed on buttonpress in file menu
func file_menu_id_pressed(id : int) -> void:
	match id:
		FileMenuId.NEW:
			on_new_project_file_menu_option_pressed()
		FileMenuId.SAVE:
			save_project_file()
		FileMenuId.LOAD:
			load_project_file()
		FileMenuId.EXPORT_AS:
			export_selection_dialog()

func on_new_project_file_menu_option_pressed() -> void:
	get_node("FileMenu/NewMapDialogue").popup_centered()

func save_project_file() -> void:
	get_node("FileMenu/SaveMapDialogue").popup_centered()

func load_project_file() -> void:
	get_node("FileMenu/LoadMapDialogue").popup_centered()

func export_selected_file_dialog() -> void:
	get_node("FileMenu/ExportMapDialogue/ExportMapLocationDialogue").popup_centered()

func export_selection_dialog() -> void:
	# fill viewport with all sprites in maproot
	var tempViewport = get_node(exportViewPort)
	exportMapRoot = get_node(MapRoot).duplicate()
	tempViewport.add_child(exportMapRoot)
	get_node("FileMenu/ExportMapDialogue/VBoxContainer/HBoxContainer/HBoxContainer/Width").text = String(tempViewport.size.x)
	get_node("FileMenu/ExportMapDialogue/VBoxContainer/HBoxContainer/HBoxContainer/Height").text = String(tempViewport.size.y)
	autocenter_and_zoom_exportcamera()
	
	#off it goes
	get_node("FileMenu/ExportMapDialogue").popup_centered()
	
func autocenter_and_zoom_exportcamera() -> void:
	print("autocenter aufgerufen")
	# set camera autocenter position dynamically
	var sprites = get_tree().get_nodes_in_group("map_elements")
	# minMaxArray = [minX, maxX, minY, maxY]
	var minMaxArray = [INF,-INF,INF,-INF]
	# do math for selfwriten Rect2 to find X and Y values
	for spritenode in sprites:
		#sprite has Rect2
		if spritenode is Sprite:
			var spriterect = spritenode.get_rect().abs()
			#local coordinate point array
			var pointarray = [spriterect.position, spriterect.position+Vector2(spriterect.size.x,0), spriterect.position+Vector2(0,spriterect.size.y), spriterect.position+spriterect.size]
			#rotate points
			for i in pointarray.size():
				pointarray[i] = spritenode.global_transform*pointarray[i]
			for point in pointarray:
				minMaxArray = min_max_from_point(minMaxArray,point)

	#camera position and zoom
	var camera = get_node("FileMenu/ExportMapDialogue/VBoxContainer/MapContainer/MapViewport/Camera")
	camera.position = Vector2(0.5*(minMaxArray[0]+minMaxArray[1]),0.5*(minMaxArray[2]+minMaxArray[3]))
	#compute camera zoom
	var x_length = minMaxArray[1] - minMaxArray[0]
	var y_length = minMaxArray[3] - minMaxArray[2]
	var x_zoom = x_length/get_node(exportViewPort).size.x
	var y_zoom = y_length/get_node(exportViewPort).size.y
	var max_zoom = max(x_zoom, y_zoom)
	camera.zoom = Vector2(max_zoom,max_zoom)

func select_exportarea_manually() -> void:
	pass

func resize_viewportwindow() -> void:
	var new_width = get_node("FileMenu/ExportMapDialogue/VBoxContainer/HBoxContainer/HBoxContainer/Width").text.to_int()
	var new_height = get_node("FileMenu/ExportMapDialogue/VBoxContainer/HBoxContainer/HBoxContainer/Height").text.to_int()
	var exportMapDialog = get_node("FileMenu/ExportMapDialogue/VBoxContainer/MapContainer/MapViewport")
	exportMapDialog.size = Vector2(new_width,new_height)
	autocenter_and_zoom_exportcamera()

func min_max_from_point(minMaxArray: Array, point: Vector2) -> Array:
	minMaxArray[0] = min(minMaxArray[0], point.x)
	minMaxArray[1] = max(minMaxArray[1], point.x)
	minMaxArray[2] = min(minMaxArray[2], point.y)
	minMaxArray[3] = max(minMaxArray[3], point.y)
	return minMaxArray

func delete_mapRoot_children() -> void:
	exportMapRoot.queue_free()

func on_mapsave_signal(file_path) -> void:
	var packedScene = PackedScene.new()
	get_node(MapRoot).owner = get_node(MapRoot).get_parent()
	packedScene.pack(get_node(MapRoot))
	var _save_error = ResourceSaver.save(file_path, packedScene)

func on_mapload_signal(filepath) -> void:
	var loadedResource = ResourceLoader.load(filepath)
	var unpackedResource = loadedResource.instance()
	get_node(MapRoot).replace_by(unpackedResource)

func export_png(filepath) -> void:
	# export
	var img = get_node(exportViewPort).get_texture().get_data()
	img.flip_y()
	img.save_png(filepath)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_ExportMapDialogue_tree_exiting():
	pass # Replace with function body.
